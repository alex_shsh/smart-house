import express from "express";
import { Client } from "pg";
import bodyParser from 'body-parser'
import routerHouses from "./routers/routerHouses";
import routerDevices from "./routers/routerDevices";

const PORT = process.env.PORT || 3000;

export const DB = new Client({
  password: "postgres",
  user: "postgres",
  host: "postgres",
});

DB.query(`
  CREATE TABLE IF NOT EXISTS houses ( 
    id serial primary key,
    name varchar(200) NOT NULL
  )
`) 

DB.query(`
  CREATE TABLE IF NOT EXISTS devices ( 
    id serial primary key,
    house_id int NOT NULL,
    name varchar(200) NOT NULL,
    working BOOLEAN NOT NULL,
    volume INT NOT NULL
  )
`)

const app = express(); 
app.use(express.urlencoded({extended: true})); 
app.use(express.json());   

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
     next();
});

app.use('/houses', routerHouses);
app.use('/devices', routerDevices);
 
(async () => {
  await DB.connect();

  app.listen(PORT, () => {
    console.log("Started at http://localhost:%d", PORT);
  });
})();
