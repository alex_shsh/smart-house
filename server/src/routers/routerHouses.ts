import express from "express";
import { DB } from "../index";
const router = express.Router(); 
  
router.get("/", async (req, res) => {
  const result = await DB.query("SELECT * FROM houses ORDER BY id DESC") 
  res.json(result['rows']);
});

router.post("/", async (req, res) => {
  const {name} = req.body;
  try { 
    const result = await DB.query('INSERT INTO houses(name) VALUES($1) RETURNING *', [name]) 
    res.json(result['rows'][0]);
  } catch (error) { 
    res.status(400).json('Something broke!');
  }
});

router.put("/", async (req, res) => {
  const {name, id} = req.body;
  try { 
    const result = await DB.query('UPDATE houses SET name = $1 WHERE id = $2 RETURNING id', [name, id]) 
    res.json(result['rows'][0]);
  } catch (error) { 
    res.status(400).json('Something broke!');
  }
});

router.delete("/", async (req, res) => {
  const {id} = req.body;
  try { 
    const result = await DB.query('DELETE FROM houses WHERE id = $1 RETURNING id', [id]) 
    res.json(result['rows'][0]);
  } catch (error) { 
    res.status(400).json('Something broke!');
  }
});

export default router