import express from "express";
import { DB } from "../index";
const router = express.Router(); 
  
router.get("/", async (req, res) => {
  const {house_id} = req.query;
  const result = await DB.query("SELECT * FROM devices WHERE house_id = $1 ORDER BY id DESC", [house_id])
 
  res.json(result['rows']);
});

router.post("/", async (req, res) => {
  const {house_id, name} = req.body;
  const [working, volume] = [false, 0]
  try {  
    const result = await DB.query(`INSERT INTO 
    devices(house_id, name, working, volume) VALUES($1, $2, $3, $4) RETURNING *`, [house_id, name, working, volume])
  
    res.json(result['rows'][0]);
  } catch (error) { 
    res.status(400).json('Something broke!');
  }
});

router.put("/", async (req, res) => {
  const {name, working, volume, id} = req.body;
  try { 
    const result = await DB.query(`UPDATE devices SET name = $1, working = $2, volume = $3
     WHERE id = $4 RETURNING id`, [name, working, volume, id]) 
    res.json(result['rows'][0]);
  } catch (error) { 
    res.status(400).json('Something broke!');
  }
});

router.delete("/", async (req, res) => {
  const {id} = req.body;
  try { 
    const result = await DB.query('DELETE FROM devices WHERE id = $1 RETURNING id', [id]) 
    res.json(result['rows'][0]);
  } catch (error) { 
    res.status(400).json('Something broke!');
  }
});

export default router