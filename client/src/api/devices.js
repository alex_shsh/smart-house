import { HEADERS, SERVER } from "./helper";

export function getDevices(house_id) {
  return fetch(`${SERVER}/devices?house_id=${house_id}`)
  .then(res => res.json())
  .then(res => res)
  .catch(er => [])
}

export function addNewDevice(data) {
  return fetch(SERVER+'/devices', { 
    method: 'POST',
    headers: HEADERS, 
    body: JSON.stringify(data)
  })
  .then(res => res.json())
  .then(res => res)
  .catch(er => null)
}

export function updateDevice(data) {
  return fetch(SERVER+'/devices', { 
    method: 'PUT',
    headers: HEADERS, 
    body: JSON.stringify(data)
  })
  .then(res => res.json())
  .then(res => res)
  .catch(er => null)
}

export function deleteDevice(data) {
  return fetch(SERVER+'/devices', { 
    method: 'DELETE',
    headers: HEADERS, 
    body: JSON.stringify(data)
  })
  .then(res => res.json())
  .then(res => res)
  .catch(er => null)
}