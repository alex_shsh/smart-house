import { HEADERS, SERVER } from "./helper";

export function getHouses() {
  return fetch(SERVER+'/houses')
  .then(res => res.json())
  .then(res => res)
  .catch(er => [])
} 

export function addNewHouse(data) {
  return fetch(SERVER+'/houses', { 
    method: 'POST',
    headers: HEADERS, 
    body: JSON.stringify(data)
  })
  .then(res => res.json())
  .then(res => res)
  .catch(er => null)
}