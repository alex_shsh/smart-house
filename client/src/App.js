import { Typography } from '@material-ui/core';
import Houses from './components/houses/Houses' 

function App() {
  return ( 
    <div className="App">  
      <Typography variant="h3" gutterBottom>
        Smart house
      </Typography>
      <Houses />
    </div> 
  );
}

export default App;
