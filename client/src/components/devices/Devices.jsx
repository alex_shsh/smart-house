import React, {useEffect, useState} from 'react';
import AccordionDetails from '@material-ui/core/AccordionDetails'; 
import { getDevices } from '../../api/devices';
import AddDevice from './AddDevice';
import Device from './Device'; 

const Devices = ({houseId}) => {
  const [devices, setDevices] = useState([])
  
  useEffect(() => {
    if(houseId){
      getDevices(houseId).then(res => setDevices(res))
    }
  }, [houseId]);
 
  return ( 
    <AccordionDetails style={{display: 'block'}}>
      <AddDevice houseId={houseId} setDevices={setDevices}/>
      {devices.map((el, i) => <Device {...el} index={i+1} key={el.id} setDevices={setDevices} />)} 
  </AccordionDetails>
  );
}

export default Devices;
