import React, {useState, useMemo} from 'react';
import { TextField, Typography, Divider } from '@material-ui/core'; 
import Button from '@material-ui/core/Button';
import style from './device.module.css';
import { addNewDevice } from '../../api/devices';

const AddDevice = ({houseId, setDevices}) => {
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);

  const handleName= e => { 
    const val = e.target.value
    setName(val)
  }

  const addDevice = () => {
    setLoading(true)
    addNewDevice({name, house_id: houseId}).then(res => { 
      if(res) setDevices(prev => [res, ...prev])
      setLoading(false)
      setName('')
    })
  }

  const hasError = useMemo(() => {
    return !name.trim().length
  }, [name])
 
  return (
    <div className={style.addDevice}> 
      <Typography variant="h5" gutterBottom>
        Add a new device
      </Typography>
      <div className={style['addDevice-panel']}> 
        <TextField label="Name" value={name} onChange={handleName} disabled={loading}/>
        <Button variant="contained" color="primary" onClick={addDevice} disabled={hasError || loading}>
          Add device
        </Button> 
      </div>
      <Divider />
    </div>
  );
}

export default AddDevice;
