import React, { useEffect, useState } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import style from './device.module.css';
import { Checkbox, FormControlLabel, Slider } from '@material-ui/core';
// api
import { updateDevice, deleteDevice } from '../../api/devices';

const Device = (props) => {
  const [state, setstate] = useState({name: '', volume: '0', working: false});

  useEffect(() => {
    setstate({name: props.name, volume: props.volume, working: props.working})
  }, []); 
  
  const handleSave = () => {
    updateDevice({...state, id: props.id})
  } 

  const handleDelete = () => {
    deleteDevice({...state, id: props.id}) 
    .then(data => {
      if(data && data.id) props.setDevices(prev => prev.filter(el => el.id !== data.id))
    })
  } 

  return (
    <Card className={style.device}>
      <CardContent>
        <Typography  color="textSecondary" gutterBottom>
          {props.index}) {state.name}
        </Typography> 
        <Slider 
          value={state.volume} 
          step={1}  
          min={0}
          max={100}
          valueLabelDisplay="on"
          onChange={(e, v) => setstate(prev => ({...prev, volume: v}))}
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={state.working}
              onChange={e => setstate(prev => ({...prev, working: e.target.checked}))}
              name="checkedB"
              color="primary"
            />
          }
          label="Working"
        />
      </CardContent>
      <CardActions>
        <Button size="small" color="primary" onClick={handleSave}>Save</Button>
        <Button size="small" onClick={handleDelete}>Delete</Button>
      </CardActions>
    </Card>
  );
}

export default Device;
