import React, {useEffect, useState} from 'react';   
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { getHouses } from '../../api/houses';
import styles from './houses.module.css';
// Components
import Devices from '../devices/Devices';
import AddHouse from './AddHouse';

/**
 * [Component]
 */
const Houses = () => {
  const [houses, setHouses] = useState([])
  const [expandedId, setExpandedId] = React.useState(null);

  useEffect(() => {
    getHouses().then(res => setHouses(res))
  }, []); 

  const handleChange = (id) => (_, isExpanded) => {
    setExpandedId(isExpanded ? id : null);
  }; 

  return (
    <div className={styles['houses']}>
      <AddHouse setHouses={setHouses} /> 
      {houses && houses.map(el => 
        <Accordion key={el.id} expanded={expandedId === el.id} onChange={handleChange(el.id)}  
          className={styles['houses-accordion']}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography>{el.name}</Typography>
          </AccordionSummary>
          <Devices houseId={expandedId === el.id ? expandedId : null} />
        </Accordion>  
      )}
    </div>
  );
}

export default Houses;
