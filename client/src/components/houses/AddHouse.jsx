import React, {useState, useMemo} from 'react';
import { TextField, Typography } from '@material-ui/core'; 
import Button from '@material-ui/core/Button'; 
import { addNewHouse } from '../../api/houses';

const AddHouse = ({houseId, setHouses}) => {
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);

  const handleName= e => { 
    const val = e.target.value
    setName(val)
  }

  const addHouse = () => {
    setLoading(true)
    addNewHouse({name, house_id: houseId}).then(res => { 
      if(res) setHouses(prev => [res, ...prev])
      setLoading(false)
      setName('')
    })
  }

  const hasError = useMemo(() => {
    return !name.trim().length
  }, [name])
 
  return (
    <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom: 30}}> 
      <Typography variant="h6" >
        Add a new house: 
      </Typography> 
      <div style={{display: 'flex', marginLeft: 20, alignItems: 'flex-end'}}>
        <TextField label="Name" value={name} onChange={handleName} disabled={loading} style={{marginRight: 10}}/>
        <Button variant="contained" color="primary" onClick={addHouse} disabled={hasError || loading}>
          Add
        </Button> 
      </div> 
    </div>
  );
}

export default AddHouse;
